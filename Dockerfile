FROM openjdk:11-jdk-slim
VOLUME /tmp
ADD target/app.jar app.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar