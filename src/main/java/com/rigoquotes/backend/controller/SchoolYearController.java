package com.rigoquotes.backend.controller;

import com.rigoquotes.backend.business.domain.dto.SchoolYearDto;
import com.rigoquotes.backend.business.domain.entity.SchoolYear;
import com.rigoquotes.backend.business.domain.response.BaseResponse;
import com.rigoquotes.backend.business.service.SchoolYearService;
import com.rigoquotes.backend.business.service.utils.ResponseBuilder;
import java.util.UUID;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/schoolYears")
public class SchoolYearController implements ICrudController<SchoolYearDto> {
    private final SchoolYearService schoolYearService;
    private final ModelMapper modelMapper;

    @Autowired
    public SchoolYearController(SchoolYearService schoolYearService, ModelMapper modelMapper) {
        this.schoolYearService = schoolYearService;
        this.modelMapper = modelMapper;
    }

    @Override
    @PostMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<UUID>> create(@RequestBody SchoolYearDto schoolYearDto) {
        SchoolYear schoolYear = modelMapper.map(schoolYearDto, SchoolYear.class);
        return ResponseBuilder.build(schoolYearService.create(schoolYear));
    }

    @Override
    @GetMapping(
        value = "/{id}",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<SchoolYearDto>> findById(@PathVariable(value = "id") UUID id) {
        BaseResponse<SchoolYear> baseResponse = schoolYearService.findById(id);
        return toBaseResponseDto(baseResponse);
    }

    @GetMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Page<SchoolYearDto>>> findAll(Pageable pageable) {
        BaseResponse<Page<SchoolYear>> baseResponse = schoolYearService.findAll(pageable);

        PageImpl page = new PageImpl(
            baseResponse
                .getResult()
                .stream()
                .map(entity -> modelMapper.map(entity, SchoolYearDto.class))
                .collect(Collectors.toList()),
            pageable,
            baseResponse.getResult().getTotalElements()
        );

        BaseResponse<Page<SchoolYearDto>> baseResponseDto = new BaseResponse<>(
            baseResponse.getResponseType(),
            baseResponse.getReason(),
            page,
            baseResponse.getHttpStatus()
        );

        return ResponseBuilder.build(baseResponseDto);
    }

    @Override
    @PutMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<SchoolYearDto>> update(@RequestBody SchoolYearDto schoolYearDto) {
        SchoolYear schoolYear = modelMapper.map(schoolYearDto, SchoolYear.class);

        BaseResponse<SchoolYear> baseResponse = schoolYearService.update(schoolYear);

        schoolYearDto = modelMapper.map(schoolYear, SchoolYearDto.class);

        BaseResponse<SchoolYearDto> baseResponseDto = new BaseResponse<>(
            baseResponse.getResponseType(),
            baseResponse.getReason(),
            schoolYearDto,
            baseResponse.getHttpStatus()
        );

        return ResponseBuilder.build(baseResponseDto);
    }

    @Override
    @DeleteMapping(
        value = "/{id}",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Void>> delete(@PathVariable(value = "id") UUID id) {
        return ResponseBuilder.build(schoolYearService.delete(id));
    }

    @Override
    @DeleteMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Void>> delete(@RequestBody SchoolYearDto schoolYearDto) {
        SchoolYear schoolYear = modelMapper.map(schoolYearDto, SchoolYear.class);
        return ResponseBuilder.build(schoolYearService.delete(schoolYear));
    }

    private ResponseEntity<BaseResponse<SchoolYearDto>> toBaseResponseDto(BaseResponse<SchoolYear> baseResponse) {
        SchoolYearDto schoolYearDto = modelMapper.map(baseResponse.getResult(), SchoolYearDto.class);

        BaseResponse<SchoolYearDto> baseResponseDto = new BaseResponse<>(
            baseResponse.getResponseType(),
            baseResponse.getReason(),
            schoolYearDto,
            baseResponse.getHttpStatus()
        );

        return ResponseBuilder.build(baseResponseDto);
    }
}

