package com.rigoquotes.backend.controller;

import com.rigoquotes.backend.business.domain.response.BaseResponse;
import java.util.UUID;

import org.springframework.http.ResponseEntity;

public interface ICrudController<T> {
    ResponseEntity<BaseResponse<UUID>> create(T t);

    ResponseEntity<BaseResponse<T>> findById(UUID id);

    ResponseEntity<BaseResponse<T>> update(T t);

    ResponseEntity<BaseResponse<Void>> delete(UUID id);

    ResponseEntity<BaseResponse<Void>> delete(T t);
}
