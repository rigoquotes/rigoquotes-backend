package com.rigoquotes.backend.controller;

import com.rigoquotes.backend.business.domain.dto.AuthorDto;
import com.rigoquotes.backend.business.domain.entity.Author;
import com.rigoquotes.backend.business.domain.response.BaseResponse;
import com.rigoquotes.backend.business.service.AuthorService;
import com.rigoquotes.backend.business.service.utils.ResponseBuilder;
import java.util.UUID;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authors")
public class AuthorController implements ICrudController<AuthorDto> {
    private final AuthorService authorService;
    private final ModelMapper modelMapper;

    @Autowired
    public AuthorController(AuthorService authorService, ModelMapper modelMapper) {
        this.authorService = authorService;
        this.modelMapper = modelMapper;
    }

    @Override
    @PostMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<UUID>> create(@RequestBody AuthorDto authorDto) {
        Author author = modelMapper.map(authorDto, Author.class);
        return ResponseBuilder.build(authorService.create(author));
    }

    @Override
    @GetMapping(
        value = "/{id}",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<AuthorDto>> findById(@PathVariable(value = "id") UUID id) {
        BaseResponse<Author> baseResponse = authorService.findById(id);
        return toBaseResponseDto(baseResponse);
    }
    
    @GetMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Page<AuthorDto>>> findAll(Pageable pageable) {
        BaseResponse<Page<Author>> baseResponse = authorService.findAll(pageable);

        PageImpl page = new PageImpl(
            baseResponse
                .getResult()
                .stream()
                .map(entity -> modelMapper.map(entity, AuthorDto.class))
                .collect(Collectors.toList()),
            pageable,
            baseResponse.getResult().getTotalElements()
        );

        BaseResponse<Page<AuthorDto>> baseResponseDto = new BaseResponse<>(
            baseResponse.getResponseType(),
            baseResponse.getReason(),
            page,
            baseResponse.getHttpStatus()
        );

        return ResponseBuilder.build(baseResponseDto);
    }

    @Override
    @PutMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<AuthorDto>> update(@RequestBody AuthorDto authorDto) {
        Author author = modelMapper.map(authorDto, Author.class);

        BaseResponse<Author> baseResponse = authorService.update(author);

        authorDto = modelMapper.map(author, AuthorDto.class);

        BaseResponse<AuthorDto> baseResponseDto = new BaseResponse<>(
            baseResponse.getResponseType(),
            baseResponse.getReason(),
            authorDto,
            baseResponse.getHttpStatus()
        );

        return ResponseBuilder.build(baseResponseDto);
    }

    @Override
    @DeleteMapping(
        value = "/{id}",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Void>> delete(@PathVariable(value = "id") UUID id) {
        return ResponseBuilder.build(authorService.delete(id));
    }

    @Override
    @DeleteMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Void>> delete(@RequestBody AuthorDto authorDto) {
        Author author = modelMapper.map(authorDto, Author.class);
        return ResponseBuilder.build(authorService.delete(author));
    }

    private ResponseEntity<BaseResponse<AuthorDto>> toBaseResponseDto(BaseResponse<Author> baseResponse) {
        AuthorDto authorDto = modelMapper.map(baseResponse.getResult(), AuthorDto.class);

        BaseResponse<AuthorDto> baseResponseDto = new BaseResponse<>(
            baseResponse.getResponseType(),
            baseResponse.getReason(),
            authorDto,
            baseResponse.getHttpStatus()
        );

        return ResponseBuilder.build(baseResponseDto);
    }
}
