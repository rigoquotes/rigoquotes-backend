package com.rigoquotes.backend.controller;

import com.rigoquotes.backend.business.domain.dto.QuoteDto;
import com.rigoquotes.backend.business.domain.entity.Quote;
import com.rigoquotes.backend.business.domain.response.BaseResponse;
import com.rigoquotes.backend.business.service.QuoteService;
import com.rigoquotes.backend.business.service.utils.ResponseBuilder;
import java.util.UUID;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/quotes")
public class QuoteController implements ICrudController<QuoteDto> {
    private final QuoteService quoteService;
    private final ModelMapper modelMapper;

    @Autowired
    public QuoteController(QuoteService quoteService, ModelMapper modelMapper) {
        this.quoteService = quoteService;
        this.modelMapper = modelMapper;
    }

    @Override
    @PostMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<UUID>> create(@RequestBody QuoteDto quoteDto) {
        Quote quote = modelMapper.map(quoteDto, Quote.class);
        return ResponseBuilder.build(quoteService.create(quote));
    }

    @Override
    @GetMapping(
        value = "/{id}",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<QuoteDto>> findById(@PathVariable(value = "id") UUID id) {
        BaseResponse<Quote> baseResponse = quoteService.findById(id);
        return toBaseResponseDto(baseResponse);
    }

    @GetMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Page<QuoteDto>>> findAll(
        Pageable pageable,
        @RequestParam(value = "isAccepted", required = false, defaultValue = "-1") int isAccepted
    ) {
        BaseResponse<Page<Quote>> baseResponse;

        switch (isAccepted) {
            case -1:
                baseResponse = quoteService.findAll(pageable);
                break;
            case 0:
                baseResponse = quoteService.findAllByAccepted(pageable, false);
                break;
            case 1:
                baseResponse = quoteService.findAllByAccepted(pageable, true);
                break;
            default:
                baseResponse = quoteService.findAll(pageable);
                break;
        }

        PageImpl page = new PageImpl(
            baseResponse
                .getResult()
                .stream()
                .map(entity -> modelMapper.map(entity, QuoteDto.class))
                .collect(Collectors.toList()),
            pageable,
            baseResponse.getResult().getTotalElements()
        );

        BaseResponse<Page<QuoteDto>> baseResponseDto = new BaseResponse<>(
            baseResponse.getResponseType(),
            baseResponse.getReason(),
            page,
            baseResponse.getHttpStatus()
        );

        return ResponseBuilder.build(baseResponseDto);
    }

    @Override
    @PutMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<QuoteDto>> update(@RequestBody QuoteDto quoteDto) {
        Quote quote = modelMapper.map(quoteDto, Quote.class);

        BaseResponse<Quote> baseResponse = quoteService.update(quote);

        quoteDto = modelMapper.map(quote, QuoteDto.class);

        BaseResponse<QuoteDto> baseResponseDto = new BaseResponse<>(
            baseResponse.getResponseType(),
            baseResponse.getReason(),
            quoteDto,
            baseResponse.getHttpStatus()
        );

        return ResponseBuilder.build(baseResponseDto);
    }

    @Override
    @DeleteMapping(
        value = "/{id}",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Void>> delete(@PathVariable(value = "id") UUID id) {
        return ResponseBuilder.build(quoteService.delete(id));
    }

    @Override
    @DeleteMapping(
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Void>> delete(@RequestBody QuoteDto quoteDto) {
        Quote quote = modelMapper.map(quoteDto, Quote.class);
        return ResponseBuilder.build(quoteService.delete(quote));
    }

    @GetMapping(
        value = "/random",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<QuoteDto>> randomQuote() {
        BaseResponse<Quote> baseResponse = quoteService.randomQuote();
        return toBaseResponseDto(baseResponse);
    }

    @PostMapping(
        value = "/{id}/like",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Void>> likeQuote(@PathVariable(value = "id") UUID quoteId) {
        return ResponseBuilder.build(quoteService.likeQuote(quoteId, UUID.randomUUID()));
    }

    @PostMapping(
        value = "/{id}/dislike",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Void>> dislikeQuote(@PathVariable(value = "id") UUID quoteId) {
        return ResponseBuilder.build(quoteService.dislikeQuote(quoteId, UUID.randomUUID()));
    }

    @DeleteMapping(
        value = "/{id}/vote",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<BaseResponse<Void>> removeVote(@PathVariable(value = "id") UUID quoteId) {
        return ResponseBuilder.build(quoteService.removeVote(quoteId, UUID.randomUUID()));
    }

    private ResponseEntity<BaseResponse<QuoteDto>> toBaseResponseDto(BaseResponse<Quote> baseResponse) {
        QuoteDto quoteDto = modelMapper.map(baseResponse.getResult(), QuoteDto.class);

        BaseResponse<QuoteDto> baseResponseDto = new BaseResponse<>(
            baseResponse.getResponseType(),
            baseResponse.getReason(),
            quoteDto,
            baseResponse.getHttpStatus()
        );

        return ResponseBuilder.build(baseResponseDto);
    }
}
