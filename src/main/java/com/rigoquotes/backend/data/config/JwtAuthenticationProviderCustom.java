package com.rigoquotes.backend.data.config;

import com.auth0.jwk.JwkProvider;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.spring.security.api.JwtAuthenticationProvider;
import com.auth0.spring.security.api.authentication.AuthenticationJsonWebToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthenticationProviderCustom extends JwtAuthenticationProvider {
    JwtAuthenticationProviderCustom(JwkProvider jwkProvider, String issuer, String audience) {
        super(jwkProvider, issuer, audience);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // Authenticating using the super method so we don't have to do anything
        Authentication auth = super.authenticate(authentication);

        // If the auth object is null return null to avoid NullPointerException
        if (auth == null) {
            return null;
        }

        // Cast the Authentification object to AuthentificationJsonWebToken to get access to all properties
        AuthenticationJsonWebToken authJwt = (AuthenticationJsonWebToken) auth;

        // Get the decoded jwt parts in an object
        DecodedJWT decoded = (DecodedJWT) authJwt.getDetails();

        // Get the permissions claim in the jwt
        Claim claimPermissions = decoded.getClaim("permissions");

        // Get the claim as an array
        String[] permissions = claimPermissions.asArray(String.class);

        // Cast the permissions to SimpleGrantedAuthority to be able to use the hasAuthority method
        List<SimpleGrantedAuthority> authorities = Arrays
            .stream(permissions)
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());

        // Return a new Authentification object as an UsernamePasswordAuthenticationToken
        // to be able to set the authorities
        return new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), authorities);
    }
}
