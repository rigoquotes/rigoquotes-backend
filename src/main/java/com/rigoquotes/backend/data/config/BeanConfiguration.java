package com.rigoquotes.backend.data.config;

import com.rigoquotes.backend.business.domain.dto.QuoteDto;
import com.rigoquotes.backend.business.domain.dto.SchoolYearDto;
import com.rigoquotes.backend.business.domain.entity.Quote;
import com.rigoquotes.backend.business.domain.entity.SchoolYear;
import com.rigoquotes.backend.business.domain.entity.Vote;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Configuration
public class BeanConfiguration {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        modelMapper = addSchoolYearMapping(modelMapper);
        modelMapper = addQuoteMapping(modelMapper);

        modelMapper.validate();

        return modelMapper;
    }

    private ModelMapper addSchoolYearMapping(ModelMapper modelMapper) {
        PropertyMap<SchoolYear, SchoolYearDto> schoolYearToDtoPropertyMap = new PropertyMap<>() {
            protected void configure() {
                map().setEndingYear(source.getEndYear());
            }
        };

        PropertyMap<SchoolYearDto, SchoolYear> schoolYearToEntityPropertyMap = new PropertyMap<>() {
            protected void configure() {
                map().setEndYear(source.getEndingYear());
            }
        };

        modelMapper.addMappings(schoolYearToDtoPropertyMap);
        modelMapper.addMappings(schoolYearToEntityPropertyMap);

        return modelMapper;
    }

    private ModelMapper addQuoteMapping(ModelMapper modelMapper) {
        PropertyMap<Quote, QuoteDto> quoteToDtoPropertyMap = new PropertyMap<>() {
            protected void configure() {
                using(context -> getLikes(
                    ((Quote) context.getSource()).getVotes())
                )
                    .map(source, destination.getLikes());

                using(context -> getDislikes(
                    ((Quote) context.getSource()).getVotes())
                )
                    .map(source, destination.getDislikes());
            }
        };

        PropertyMap<QuoteDto, Quote> quoteToEntityPropertyMap = new PropertyMap<>() {
            protected void configure() {
                map().setVotes(null);
            }
        };

        modelMapper.addMappings(quoteToDtoPropertyMap);
        modelMapper.addMappings(quoteToEntityPropertyMap);

        return modelMapper;
    }

    private Long getLikes(Set<Vote> votes) {
        return votes.stream().filter(Vote::isLike).count();
    }

    private Long getDislikes(Set<Vote> votes) {
        return votes.stream().filter(vote -> !vote.isLike()).count();
    }
}
