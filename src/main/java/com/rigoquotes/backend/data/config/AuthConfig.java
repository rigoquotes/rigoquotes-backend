package com.rigoquotes.backend.data.config;

import com.auth0.jwk.JwkProviderBuilder;
import com.auth0.spring.security.api.JwtWebSecurityConfigurer;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class AuthConfig extends WebSecurityConfigurerAdapter {
    @Value(value = "${auth0.apiAudience}")
    private String apiAudience;
    @Value(value = "${auth0.issuer}")
    private String issuer;

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("https://*.rigoquotes.fr"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST", "PUT", "DELETE"));
        configuration.setAllowCredentials(true);
        configuration.addAllowedHeader("Authorization");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable();
        JwtWebSecurityConfigurer
            .forRS256(apiAudience, issuer, new JwtAuthenticationProviderCustom(new JwkProviderBuilder(issuer).build(), issuer, apiAudience))
            .configure(http)
            .authorizeRequests()
            .antMatchers(HttpMethod.POST, "/authors").hasAuthority("write:authors")
            .antMatchers(HttpMethod.GET, "/authors/**").hasAuthority("read:authors")
            .antMatchers(HttpMethod.GET, "/authors").hasAuthority("read:authors")
            .antMatchers(HttpMethod.PUT, "/authors").hasAuthority("update:authors")
            .antMatchers(HttpMethod.DELETE, "/authors/**").hasAuthority("delete:authors")
            .antMatchers(HttpMethod.DELETE, "/authors").hasAuthority("delete:authors")
            .antMatchers(HttpMethod.POST, "/schoolYears").hasAuthority("write:schoolYears")
            .antMatchers(HttpMethod.GET, "/schoolYears/**").hasAuthority("read:schoolYears")
            .antMatchers(HttpMethod.GET, "/schoolYears").hasAuthority("read:schoolYears")
            .antMatchers(HttpMethod.PUT, "/schoolYears").hasAuthority("update:schoolYears")
            .antMatchers(HttpMethod.DELETE, "/schoolYears/**").hasAuthority("delete:schoolYears")
            .antMatchers(HttpMethod.DELETE, "/schoolYears").hasAuthority("delete:schoolYears")
            .antMatchers(HttpMethod.POST, "/quotes").hasAuthority("write:quotes")
            .antMatchers(HttpMethod.GET, "/quotes/**").permitAll()
            .antMatchers(HttpMethod.GET, "/quotes").permitAll()
            .antMatchers(HttpMethod.PUT, "/quotes").hasAuthority("update:quotes")
            .antMatchers(HttpMethod.DELETE, "/quotes/**").hasAuthority("delete:quotes")
            .antMatchers(HttpMethod.DELETE, "/quotes").hasAuthority("delete:quotes")
            .antMatchers(HttpMethod.GET, "/quotes/random").permitAll()
            .antMatchers(HttpMethod.POST, "/quotes/**/like").hasAuthority("like:quotes")
            .antMatchers(HttpMethod.POST, "/quotes/**/dislike").hasAuthority("dislike:quotes")
            .antMatchers(HttpMethod.DELETE, "/quotes/**/vote").hasAuthority("remove_vote:quotes")
        .and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
