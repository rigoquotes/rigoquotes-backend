package com.rigoquotes.backend.data.repository;

import com.rigoquotes.backend.business.domain.entity.SchoolYear;
import java.util.UUID;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolYearRepository extends PagingAndSortingRepository<SchoolYear, UUID> {
}
