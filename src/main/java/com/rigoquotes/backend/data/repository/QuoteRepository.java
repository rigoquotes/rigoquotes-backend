package com.rigoquotes.backend.data.repository;

import com.rigoquotes.backend.business.domain.entity.Quote;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuoteRepository extends PagingAndSortingRepository<Quote, UUID> {
    @Query("select p.id from Quote p")
    List<UUID> getAllIds();

    Page<Quote> findAllByAcceptedIsTrue(Pageable pageable);

    Page<Quote> findAllByAcceptedIsFalse(Pageable pageable);
}
