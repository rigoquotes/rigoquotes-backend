package com.rigoquotes.backend.business.domain.enums;

public enum  School {
    AIX_EN_PROVENCE,
    NICE,
    MONTPELLIER,
    TOULOUSE,
    PAU,
    BORDEAUX,
    GRENOBLE,
    LYON,
    ANGOULEME,
    LA_ROCHELLE,
    DIJON,
    NANTES,
    ST_NAZAIRE,
    LE_MANS,
    ORLEANS,
    STRASBOURG,
    NANCY,
    REIMS,
    PARIS_NANTERRE,
    BREST,
    CAEN,
    ROUENS,
    ARRAS,
    LILLE
}
