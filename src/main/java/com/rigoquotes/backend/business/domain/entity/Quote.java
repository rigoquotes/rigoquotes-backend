package com.rigoquotes.backend.business.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rigoquotes.backend.business.domain.enums.Promotion;
import com.rigoquotes.backend.business.domain.enums.School;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

@Entity
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Quote extends BaseEntity {
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(nullable = false)
    private String message;

    @Column(nullable = false)
    private LocalDateTime creationDate;

    @Column(nullable = false)
    private boolean isAccepted;

    @Enumerated
    @Column(nullable = false)
    private Promotion promotion;

    @Enumerated
    @Column(nullable = false)
    private School school;

    @ManyToMany(mappedBy = "quotes")
    private Set<Author> authors;

    @OneToMany(mappedBy = "quote")
    private Set<Vote> votes;

    @Column(nullable = false)
    private String creator;

    @ManyToOne(optional = false)
    private SchoolYear schoolYear;
}
