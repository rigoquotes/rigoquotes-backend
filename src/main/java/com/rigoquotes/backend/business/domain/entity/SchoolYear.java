package com.rigoquotes.backend.business.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SchoolYear extends BaseEntity {
    @Column(nullable = false)
    private int beginningYear;

    @Column(nullable = false)
    private int endYear;
}
