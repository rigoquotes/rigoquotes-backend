package com.rigoquotes.backend.business.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@IdClass(VoteId.class)
public class Vote {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    private Quote quote;

    @Id
    private UUID userId;

    @EqualsAndHashCode.Exclude
    private boolean isLike;
}
