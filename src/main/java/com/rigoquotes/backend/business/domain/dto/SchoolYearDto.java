package com.rigoquotes.backend.business.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.UUID;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SchoolYearDto {
    private UUID id;
    private int beginningYear;
    private int endingYear;
}
