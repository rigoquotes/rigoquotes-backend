package com.rigoquotes.backend.business.domain.enums;

public enum ResponseType {
    SUCCESS, ERROR
}
