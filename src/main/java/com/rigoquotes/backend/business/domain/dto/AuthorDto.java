package com.rigoquotes.backend.business.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.UUID;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AuthorDto {
    private UUID id;
    private String firstName;
    private String lastName;
}
