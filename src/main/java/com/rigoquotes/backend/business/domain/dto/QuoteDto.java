package com.rigoquotes.backend.business.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QuoteDto {
    private UUID id;
    private String message;
    private LocalDateTime creationDate;
    private boolean isAccepted;
    private String promotion;
    private String school;
    private Set<AuthorDto> authors;
    private String creator;
    private SchoolYearDto schoolYear;
    private Long likes;
    private Long dislikes;
}
