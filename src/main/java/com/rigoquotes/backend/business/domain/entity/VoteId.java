package com.rigoquotes.backend.business.domain.entity;

import java.io.Serializable;
import java.util.UUID;

public class VoteId implements Serializable {
    private Quote quote;
    private UUID userId;
}
