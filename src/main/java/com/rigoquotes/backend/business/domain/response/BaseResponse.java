package com.rigoquotes.backend.business.domain.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.rigoquotes.backend.business.domain.enums.ResponseType;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse<T> {
    private ResponseType responseType;
    private String reason;
    private T result;
    @JsonIgnore
    private HttpStatus httpStatus;

    public BaseResponse() {
        this.responseType = ResponseType.SUCCESS;
        this.reason = null;
        this.result = null;
        this.httpStatus = HttpStatus.OK;
    }

    public BaseResponse(String reason, HttpStatus httpStatus) {
        this.responseType = ResponseType.ERROR;
        this.reason = reason;
        this.result = null;
        this.httpStatus = httpStatus;
    }

    public BaseResponse(T result) {
        this.responseType = ResponseType.SUCCESS;
        this.reason = null;
        this.result = result;
        this.httpStatus = HttpStatus.OK;
    }

    public BaseResponse(T result, HttpStatus httpStatus) {
        this.responseType = ResponseType.SUCCESS;
        this.reason = null;
        this.result = result;
        this.httpStatus = httpStatus;
    }
}
