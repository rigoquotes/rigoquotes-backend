package com.rigoquotes.backend.business.domain.enums;

public enum  Role {
    ADMINISTRATOR,
    MODERATOR,
    USER
}
