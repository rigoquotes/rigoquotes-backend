package com.rigoquotes.backend.business.service.crud;

import com.rigoquotes.backend.business.domain.entity.BaseEntity;
import com.rigoquotes.backend.business.domain.response.BaseResponse;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

public class CrudService<T extends BaseEntity> implements ICrudService<T> {
    private final PagingAndSortingRepository<T, UUID> repository;
    private final String entityName;

    public CrudService(
        PagingAndSortingRepository<T, UUID> repository,
        String entityName
    ) {
        this.repository = repository;
        this.entityName = entityName;
    }

    @Override
    public BaseResponse<UUID> create(T t) {
        if (t.getId() != null && repository.existsById(t.getId())) {
            return new BaseResponse<>(
                String.format(
                    "%s with id %s already exists.",
                    StringUtils.capitalize(entityName),
                    t.getId()
                ),
                HttpStatus.CONFLICT
            );
        } else {
            return new BaseResponse<>(
                repository.save(t).getId(),
                HttpStatus.CREATED
            );
        }
    }

    @Override
    public BaseResponse<T> findById(UUID id) {
        Optional<T> found = repository.findById(id);

        if (found.isPresent()) {
            return new BaseResponse<>(found.get());
        } else {
            return new BaseResponse<>(
                String.format("No %s found with id %s.", entityName, id),
                HttpStatus.NOT_FOUND
            );
        }
    }

    @Override
    public BaseResponse<Page<T>> findAll(Pageable pageable) {
        return new BaseResponse<>(repository.findAll(pageable));
    }

    @Override
    public BaseResponse<T> update(T t) {
        if (t.getId() == null) {
            return new BaseResponse<>(
                String.format("No id found to update %s.", entityName),
                HttpStatus.BAD_REQUEST
            );
        } else if (!repository.existsById(t.getId())) {
            return new BaseResponse<>(
                String.format(
                    "%s with id %s doesn't exist.",
                    StringUtils.capitalize(entityName),
                    t.getId()
                ),
                HttpStatus.NOT_FOUND
            );
        } else {
            return new BaseResponse<>(repository.save(t));
        }
    }

    @Override
    public BaseResponse<Void> delete(UUID id) {
        if (!repository.existsById(id)) {
            return new BaseResponse<>(
                String.format(
                    "%s with id %s doesn't exist.",
                    StringUtils.capitalize(entityName),
                    id
                ),
                HttpStatus.NOT_FOUND
            );
        } else {
            repository.deleteById(id);
            return new BaseResponse<>();
        }
    }

    @Override
    public BaseResponse<Void> delete(T t) {
        if (t.getId() == null) {
            return new BaseResponse<>(
                String.format("No id found to delete %s.", entityName),
                HttpStatus.BAD_REQUEST
            );
        } else if (!repository.existsById(t.getId())) {
            return new BaseResponse<>(
                String.format(
                    "%s with id %s doesn't exist.",
                    StringUtils.capitalize(entityName),
                    t.getId()
                ),
                HttpStatus.NOT_FOUND
            );
        } else {
            repository.delete(t);
            return new BaseResponse<>();
        }
    }
}
