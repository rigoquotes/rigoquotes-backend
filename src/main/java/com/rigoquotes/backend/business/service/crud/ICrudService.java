package com.rigoquotes.backend.business.service.crud;

import com.rigoquotes.backend.business.domain.entity.BaseEntity;
import com.rigoquotes.backend.business.domain.response.BaseResponse;
import java.util.Collection;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ICrudService<T extends BaseEntity> {
    BaseResponse<UUID> create(T t);

    BaseResponse<T> findById(UUID id);

    BaseResponse<Page<T>> findAll(Pageable pageable);

    BaseResponse<T> update(T t);

    BaseResponse<Void> delete(UUID id);

    BaseResponse<Void> delete(T t);
}
