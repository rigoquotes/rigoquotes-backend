package com.rigoquotes.backend.business.service;

import com.rigoquotes.backend.business.domain.entity.Author;
import com.rigoquotes.backend.business.domain.response.BaseResponse;
import com.rigoquotes.backend.business.service.crud.CrudService;
import com.rigoquotes.backend.business.service.crud.ICrudService;
import com.rigoquotes.backend.data.repository.AuthorRepository;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AuthorService implements ICrudService<Author> {
    private final CrudService<Author> crudService;

    @Autowired
    public AuthorService(AuthorRepository authorRepository) {
        this.crudService = new CrudService<>(authorRepository, "author");
    }

    @Override
    public BaseResponse<UUID> create(Author author) {
        return crudService.create(author);
    }

    @Override
    public BaseResponse<Author> findById(UUID id) {
        return crudService.findById(id);
    }

    @Override
    public BaseResponse<Page<Author>> findAll(Pageable pageable) {
        return crudService.findAll(pageable);
    }

    @Override
    public BaseResponse<Author> update(Author author) {
        return crudService.update(author);
    }

    @Override
    public BaseResponse<Void> delete(UUID id) {
        return crudService.delete(id);
    }

    @Override
    public BaseResponse<Void> delete(Author author) {
        return crudService.delete(author);
    }
}
