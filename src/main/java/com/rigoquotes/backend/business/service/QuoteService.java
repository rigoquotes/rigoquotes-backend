package com.rigoquotes.backend.business.service;

import com.rigoquotes.backend.business.domain.entity.Quote;
import com.rigoquotes.backend.business.domain.entity.Vote;
import com.rigoquotes.backend.business.domain.enums.ResponseType;
import com.rigoquotes.backend.business.domain.response.BaseResponse;
import com.rigoquotes.backend.business.service.crud.CrudService;
import com.rigoquotes.backend.business.service.crud.ICrudService;
import com.rigoquotes.backend.data.repository.QuoteRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class QuoteService implements ICrudService<Quote> {
    private final CrudService<Quote> crudService;
    private final QuoteRepository quoteRepository;
    private final UserService userService;

    @Autowired
    public QuoteService(QuoteRepository quoteRepository, UserService userService) {
        this.crudService = new CrudService<>(quoteRepository, "quote");
        this.quoteRepository = quoteRepository;
        this.userService = userService;
    }

    @Override
    public BaseResponse<UUID> create(Quote quote) {
        quote.setCreator(userService.getCurrentUser());
        return crudService.create(quote);
    }

    @Override
    public BaseResponse<Quote> findById(UUID id) {
        return crudService.findById(id);
    }

    @Override
    public BaseResponse<Page<Quote>> findAll(Pageable pageable) {
        return crudService.findAll(pageable);
    }

    @Override
    public BaseResponse<Quote> update(Quote quote) {
        return crudService.update(quote);
    }

    @Override
    public BaseResponse<Void> delete(UUID id) {
        if (!quoteRepository.existsById(id)) {
            return new BaseResponse<>(
                String.format(
                    "%s with id %s doesn't exist.",
                    "Quote",
                    id
                ),
                HttpStatus.NOT_FOUND
            );
        } else {
            Quote quote = findById(id).getResult();

            if (quote.getCreator().equals(userService.getCurrentUser()) || userService.isCurrentUserSuperUser()) {
                quoteRepository.deleteById(id);
                return new BaseResponse<>();
            } else {
                return new BaseResponse<>(
                    "You are not allowed to delete this ressource.",
                    HttpStatus.FORBIDDEN
                );
            }
        }
    }

    @Override
    public BaseResponse<Void> delete(Quote quote) {
        if (quote.getId() == null) {
            return new BaseResponse<>(
                String.format("No id found to delete %s.", "Quote"),
                HttpStatus.BAD_REQUEST
            );
        } else if (!quoteRepository.existsById(quote.getId())) {
            return new BaseResponse<>(
                String.format(
                    "%s with id %s doesn't exist.",
                    StringUtils.capitalize("Quote"),
                    quote.getId()
                ),
                HttpStatus.NOT_FOUND
            );
        } else {
            quote = findById(quote.getId()).getResult();

            if (quote.getCreator().equals(userService.getCurrentUser()) || userService.isCurrentUserSuperUser()) {
                quoteRepository.delete(quote);
                return new BaseResponse<>();
            } else {
                return new BaseResponse<>(
                    "You are not allowed to delete this ressource.",
                    HttpStatus.FORBIDDEN
                );
            }
        }
    }

    public BaseResponse<Quote> randomQuote() {
        List<UUID> uuids = quoteRepository.getAllIds();

        if (uuids.size() == 0) {
            return new BaseResponse<>(
                "No quotes available for operation.",
                HttpStatus.INTERNAL_SERVER_ERROR
            );
        } else if (uuids.size() == 1) {
            return findById(uuids.get(0));
        } else {
            UUID chosenUuid = uuids.get(ThreadLocalRandom.current().nextInt(uuids.size()));
            return findById(chosenUuid);
        }
    }

    public BaseResponse<Page<Quote>> findAllByAccepted(Pageable pageable, boolean isAccepted) {
        if (isAccepted) {
            return new BaseResponse<>(quoteRepository.findAllByAcceptedIsTrue(pageable));
        } else {
            return new BaseResponse<>(quoteRepository.findAllByAcceptedIsFalse(pageable));
        }
    }

    public BaseResponse<Void> likeQuote(UUID quoteId, UUID userId) {
        return castVoteQuote(quoteId, userId, true);
    }

    public BaseResponse<Void> dislikeQuote(UUID quoteId, UUID userId) {
        return castVoteQuote(quoteId, userId, false);
    }

    public BaseResponse<Void> removeVote(UUID quoteId, UUID userId) {
        BaseResponse<Quote> baseResponse = findById(quoteId);

        if (baseResponse.getResponseType() == ResponseType.ERROR) {
            return new BaseResponse<>(
                baseResponse.getReason(),
                baseResponse.getHttpStatus()
            );
        } else {
            Quote quote = baseResponse.getResult();

            Set<Vote> votes = quote.getVotes();

            if (votes == null) {
                return new BaseResponse<>(
                    String.format("The quote %s has no votes.", quoteId),
                    HttpStatus.BAD_REQUEST
                );
            }

            votes.removeIf(vote -> vote.getUserId() == userId);

            quote.setVotes(votes);

            baseResponse = update(quote);

            if (baseResponse.getResponseType() == ResponseType.SUCCESS) {
                return new BaseResponse<>();
            } else {
                return new BaseResponse<>(
                    baseResponse.getReason(),
                    baseResponse.getHttpStatus()
                );
            }
        }
    }

    private BaseResponse<Void> castVoteQuote(UUID quoteId, UUID userId, boolean isLike) {
        BaseResponse<Quote> baseResponse = findById(quoteId);

        if (baseResponse.getResponseType() == ResponseType.ERROR) {
            return new BaseResponse<>(
                baseResponse.getReason(),
                baseResponse.getHttpStatus()
            );
        } else {
            Quote quote = baseResponse.getResult();

            Set<Vote> votes = quote.getVotes();

            if (votes == null) {
                votes = new HashSet<>();
            }

            if (votes.stream().anyMatch(vote -> vote.getUserId() == userId)) {
                return new BaseResponse<>(
                    String.format("The user %s has already voted for the quote %s.", userId, quoteId),
                    HttpStatus.CONFLICT
                );
            }

            votes.add(new Vote(quote, userId, isLike));

            quote.setVotes(votes);

            baseResponse = update(quote);

            if (baseResponse.getResponseType() == ResponseType.SUCCESS) {
                return new BaseResponse<>();
            } else {
                return new BaseResponse<>(
                    baseResponse.getReason(),
                    baseResponse.getHttpStatus()
                );
            }
        }
    }
}
