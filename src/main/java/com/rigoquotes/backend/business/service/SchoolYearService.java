package com.rigoquotes.backend.business.service;

import com.rigoquotes.backend.business.domain.entity.SchoolYear;
import com.rigoquotes.backend.business.domain.response.BaseResponse;
import com.rigoquotes.backend.business.service.crud.CrudService;
import com.rigoquotes.backend.business.service.crud.ICrudService;
import com.rigoquotes.backend.data.repository.SchoolYearRepository;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SchoolYearService implements ICrudService<SchoolYear> {
    private final CrudService<SchoolYear> crudService;

    @Autowired
    public SchoolYearService(SchoolYearRepository schoolYearRepository) {
        this.crudService = new CrudService<>(schoolYearRepository, "school year");
    }

    @Override
    public BaseResponse<UUID> create(SchoolYear schoolYear) {
        return crudService.create(schoolYear);
    }

    @Override
    public BaseResponse<SchoolYear> findById(UUID id) {
        return crudService.findById(id);
    }

    @Override
    public BaseResponse<Page<SchoolYear>> findAll(Pageable pageable) {
        return crudService.findAll(pageable);
    }

    @Override
    public BaseResponse<SchoolYear> update(SchoolYear schoolYear) {
        return crudService.update(schoolYear);
    }

    @Override
    public BaseResponse<Void> delete(UUID id) {
        return crudService.delete(id);
    }

    @Override
    public BaseResponse<Void> delete(SchoolYear schoolYear) {
        return crudService.delete(schoolYear);
    }
}
