package com.rigoquotes.backend.business.service.utils;

import com.rigoquotes.backend.business.domain.response.BaseResponse;
import lombok.experimental.UtilityClass;
import org.springframework.http.ResponseEntity;

@UtilityClass
public class ResponseBuilder {
    public <T> ResponseEntity<BaseResponse<T>> build(BaseResponse<T> baseResponse) {
        return ResponseEntity.status(baseResponse.getHttpStatus()).body(baseResponse);
    }
}
